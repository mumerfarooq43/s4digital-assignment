# Getting started with REST API testing with Serenity and Cucumber 6
This tutorial show you how to get started with REST-API testing using Serenity and Cucumber 6.

## Prerequisites

You should have `jdk 8` or greater version installed on your machine.

You should have `maven 3` or higher version installed on your machine

## Note
Please use stable releases for maven and java jdk. For example maven version is 3.3.9 and JDK version is 8
`Jdk 16` is not compatible please not used

## Get the code

Git:

    git clone git@gitlab.com:mumerfarooq43/s4digital-assignment.git
    cd s4digital-assignment

## Use Maven
**Open your ide and select the pom.xml file open as project**

## To run the test
Open a command window and run:

``` mvn clean verify ```

## Use Gradle
Open a command window and run:

``` gradlew test  ```


## Viewing the reports
Both of the commands provided above will produce a Serenity test report in the `target/site/serenity` directory and open `index.html` file in your browser!


### The project directory structure
The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:

```Gherkin
src
+ main
+ test
    + java                                Test runners and supporting code
    + resources
        + features                          Feature files
            + covid

```


## A simple GET scenario
The project comes with two simple scenarios, one that illustrates a GET

The first scenario exercises the `/country?name=pakistan` endpoint
Sample project uses the sample Cucumber scenario.
In this scenario, I'm using open source API to get the COVID-19 data information by country name

```Gherkin
  Scenario: User have covid-19 data api
    Given Open source Covid API
    When User given valid params and headers
    Then The API should return "Pakistan" covid data
```

The glue code for this scenario illustrates the layered approach we find works well for both web and non-web acceptance tests.
The glue code is responsible for orchestrating calls to a layer of more business-focused classes, which perform the actual REST calls.

```java
    @Steps
    CovidApiStep covidApiStep;

    @Given("Open source Covid API")
    public void open_source_covid_api() {

        }

    @When("User given valid params and headers")
    public void valid_params_headers(){
        covidApiStep.getCovidData();
        }

```

The actual REST calls are performed using RestAssured in the action classes, like `CovidApiStep` here.

```java
@Step
public void getCovidData() {
    SerenityRest.given()
        .contentType("application/json")
        .header("x-rapidapi-key", "your API key")
        .param("name", "pakistan")
        .get(COVID.getUrl());
    }

```

In steps that perform assertions, we can also use the `SerenityRest.restAssuredThat()` helper method,
which lets us make a RestAssured assertion on the last response the server sent us:

```java
    @Then("The API should return {string} covid data")
    public void the_API_should_return_covid_data(String expectedMessage) {
        restAssuredThat(response -> response.statusCode(200).body("country", contains(expectedMessage)));
        }
```
