Feature: Get COVID-19 Data by Country Name

  Scenario: User have covid-19 data api
    Given Open source Covid API
    When User given valid params and headers
    Then The API should return "Pakistan" covid data

  Scenario: User have covid-19 data api
    Given Open source Covid API
    When User given valid params and headers with the country name is exist
    Then The API should return empty array with success response

  Scenario: User have covid-19 data api
    Given Open source Covid API
    When User is not given valid param and header
    Then The API should unauthorized access with message "Invalid API key. Go to https://docs.rapidapi.com/docs/keys for more info."

  Scenario: User have covid-19 data api
    Given Open source Covid API
    When User is not given valid param
    Then The API should bad request access with message "Bad Request"

  Scenario: User have covid-19 data api
    Given Open source Covid API
    When User is not given valid header but given param
    Then The API should unauthorized access with message "Invalid API key. Go to https://docs.rapidapi.com/docs/keys for more info."

  Scenario: User have covid-19 data api
    Given Open source Covid API
    When User changed the method from GET to POST
    Then The API should returned 404 Not Found "Endpoint/country does not exist"