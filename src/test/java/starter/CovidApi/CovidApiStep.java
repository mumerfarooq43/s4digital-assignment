package starter.CovidApi;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static starter.WebServiceEndPoints.COVID;

public class CovidApiStep {

    @Step
    public void getCovidData() {
        SerenityRest.given()
                .contentType("application/json")
                .header("x-rapidapi-key", "8cfbb188d9msh4b60e1bf6d2ccefp184853jsn07af520d6400")
                .param("name", "pakistan")
                .get(COVID.getUrl());
    }

    @Step
    public void getEmptyList() {
        SerenityRest.given()
                .contentType("application/json")
                .header("x-rapidapi-key", "8cfbb188d9msh4b60e1bf6d2ccefp184853jsn07af520d6400")
                .param("name", "test")
                .get(COVID.getUrl());
    }

    @Step
    public void withoutParamsWithHeaders() {
        SerenityRest.given()
                .contentType("application/json")
                .header("x-rapidapi-key", "8cfbb188d9msh4b60e1bf6d2ccefp184853jsn07af520d6400")
                .get(COVID.getUrl());
    }

    @Step
    public void withParamsWithoutHeaders() {
        SerenityRest.given()
                .contentType("application/json")
                .param("name", "pakistan")
                .get(COVID.getUrl());
    }

    @Step
    public void withoutHeadersParams() {
        SerenityRest.given()
                .get(COVID.getUrl());
    }

    @Step
    public void methodChanged() {
        SerenityRest.given()
                .contentType("application/json")
                .header("x-rapidapi-key", "8cfbb188d9msh4b60e1bf6d2ccefp184853jsn07af520d6400")
                .param("name", "pakistan")
                .post(COVID.getUrl());
    }

}
