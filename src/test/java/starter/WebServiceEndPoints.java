package starter;

public enum WebServiceEndPoints {
    COVID("https://covid-19-data.p.rapidapi.com/country");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
