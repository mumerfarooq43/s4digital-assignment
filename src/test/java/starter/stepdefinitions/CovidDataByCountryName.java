package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.CovidApi.CovidApiStep;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class CovidDataByCountryName {

    @Steps
    CovidApiStep covidApiStep;

    @Given("Open source Covid API")
    public void open_source_covid_api() {

    }

    @When("User given valid params and headers")
    public void valid_params_headers(){
        covidApiStep.getCovidData();
    }

    @Then("The API should return {string} covid data")
    public void the_API_should_return_covid_data(String expectedMessage) {
        restAssuredThat(response -> response.statusCode(200).body("country", contains(expectedMessage)));
    }

    @When("User is not given valid param and header")
    public void not_given_params_headers(){
        covidApiStep.withoutHeadersParams();
    }

    @Then("The API should unauthorized access with message {string}")
    public void the_api_unauthorized_access(String expectedMessage) {
        restAssuredThat(response -> response.statusCode(401).body("message", equalTo(expectedMessage)));
    }

    @When("User is not given valid param")
    public void not_given_params(){
        covidApiStep.withoutParamsWithHeaders();
    }

    @Then("The API should bad request access with message {string}")
    public void the_api_bad_request_access(String detailMessage) {
        restAssuredThat(response -> response.statusCode(400).body("detail", equalTo(detailMessage)));
    }

    @When("User is not given valid header but given param")
    public void not_given_header(){
        covidApiStep.withParamsWithoutHeaders();
    }

    @When("User changed the method from GET to POST")
    public void method_change_get_to_post(){
        covidApiStep.methodChanged();
    }

    @Then("The API should returned 404 Not Found {string}")
    public void api_should_returned_404_response(String expectedMessage) {
        restAssuredThat(response -> response.statusCode(404).body("message", equalTo(expectedMessage)));
    }

    @When("User given valid params and headers with the country name is exist")
    public void valid_params_headers_country_not_exist(){
        covidApiStep.getEmptyList();
    }

    @Then("The API should return empty array with success response")
    public void the_API_should_return_success_response() {
        restAssuredThat(response -> response.statusCode(200));
    }
}
